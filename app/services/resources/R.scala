package services.resources

object R {

  object query {
    val FILTER_QUERY = "filter"
  }

  object id {
    val ID = "id"

    val NAME_USER = "name"
    val EMAIL_USER = "email"
    val PHONE_USER = "phone"
    val PASSWORD_USER = "password"

    val PROJECT_ID_MEMBER = "project_id"
    val ROLE_MEMBER = "role"
    val USER_ID_MEMBER = "user_id"

    val NAME_PROJECT = "name"
    val DESCRIPRION_PROJECT = "description"
    val MONEY_PROJECT = "money"
    val BUDGET_PROJECT = "budget"
    val COUNT_USERS_PROJECT = "count_users"
    val TOWN_PROJECT = "town"
    val WHO_CREATED_ID_PROJECT = "who_created_id"
    val WHO_CREATED_EMAIL_PROJECT = "who_created_email"

    val OWNER_ID_POST = "ownerId"
    val PUBLIC_STATUS_POST = "publicStatus"
    val FROM_GROUP_STATUS_POST = "fromGroup"
    val PUBLISH_DATE_POST = "publishDate"
    val MESSAGE_POST = "message"
  }


  object roles {
    val CREATOR = "creator"
  }

  object auth {
    val AUTH = "auth"
    val NAME = "name"
    val PASSWORD = "password"

    val WAS_AUTHORIZATION = "was_authorization"
  }

  object tables {
    val USERS_TABLE = "user"
    val PROJECT_TABLE = "project"
    val MEMBER_TABLE = "member"
    val POST_TABLE = "post"
  }

}
