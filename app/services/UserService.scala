package services



import java.util.{Calendar, Date}

import models.{User, UserTable}
import models.SQLHelper._
import models.auth.{Generator, OAuth}
import models.json.{JsonParseObject, JsonUtil, Path}
import play.api.db.slick._
import play.api.libs.json.{JsObject, JsValue, Json}
import services.resources.R
import slick.driver.MySQLDriver.api._

import scala.collection.mutable._
import scala.concurrent.Future

case class Pack(date: Date, user: User)

object
UserService extends JsonParseObject[User] {
  var users = TableQuery[UserTable]

  private val userById = for(id <- Parameters[Long]; u <- users if u.id === id) yield u


  def add(user: User) = {
    dbConfig.db.run(users += user)
  }

  def getByName(name: Option[String]) = {
    name map { it =>
      dbConfig.db.run(users.filter(_.name === name).result)
    }
  }

  def getById(id: Long) = {
    dbConfig.db.run(userById(id).result)
  }

  /* After auth */
  def delete(token: String) = {
    val user = OAuth.getByToken(token)
    user.map { (u) =>
      users.filter(_.id === u.id).delete
    }
  }

  def getAllUsers(filter: Option[String]) = {
    filter.map { it =>
      dbConfig.db.run(
        users.filter(_.name like it + "%").result
      )
    } getOrElse dbConfig.db.run(users.result)
  }

  override def toJson(obj: User): JsValue =
    Json.obj(
      R.id.ID -> obj.id,
      R.id.NAME_USER -> obj.name,
      R.id.EMAIL_USER -> obj.email
    )


  override def fromJson(js: JsValue): User = {
    JsonUtil.parseRequestString(js)(Array(
      Path(R.id.NAME_USER),
      Path(R.id.EMAIL_USER),
      Path(R.id.PHONE_USER),
      Path(R.id.PASSWORD_USER)
    )) match {
      case Array(_2, _3, _4, _5) => User(0, _2.as[String], _3.as[String], _4.as[String], _5.as[String])
    }

  }
}
