package services

object Log {
  def i(logger: Any) = {
    System.out.println("[INFO] " + logger)
    logger
  }
}
