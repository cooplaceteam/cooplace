package services

import models.json.{JsonParseObject, JsonUtil, Path}
import models._
import play.api.libs.json.{JsValue, Json}
import services.resources.R
import slick.driver.MySQLDriver.api._

import scala.concurrent._
import ExecutionContext.Implicits.global


object ProjectService extends CommandService with CrowdService with JsonParseObject[Project] {
  var projects = TableQuery[ProjectTable]
  var members = TableQuery[MemberTable]

  private val projectById = for(id <- Parameters[Long]; p <- projects if p.id === id) yield p

  def getProjects() = SQLHelper.dbConfig.db.run(projects.sortBy(_.id).result)

  def addProject(project: Project) =
    SQLHelper.dbConfig.db.run(
      (projects returning projects.map(_.id)) += project
    )

  def setProjectInfo(projectId: Long, newProject: Project) = SQLHelper.dbConfig.db.run(
    projectById(projectId).update(newProject)
  )
  def getProjectById(projectId: Long) = SQLHelper.dbConfig.db.run(projectById(projectId).result)

  def changeCountUsers(project: Project) =
    SQLHelper.dbConfig.db.run(
      projects.filter(_.id === project.id).map(x => (x.count_user)).update(project.countUser)
    )

  def getCountUser(projectId: Long) = for(project <- projects if project.id === projectId) yield project.count_user

  def setCountUser(projectId: Long, count: Int) =
    SQLHelper.dbConfig.db.run(
      getCountUser(projectId).update(count)
    )


  def inCommand(descr: String)(projectId: Long, user: User) =
    SQLHelper.dbConfig.db.run(
      members += (projectId, "", user.id)
    ) flatMap { p =>
      SQLHelper.dbConfig.db.run(
        projects.filter(_.id === projectId).map(_.count_user).result.headOption
      )
    }

  def outCommand(projectId: Long, user: User) =
    SQLHelper.dbConfig.db.run(
      members.filter((mem) => mem.user_id === user.id && mem.project_id === projectId).delete
    ) flatMap { p =>
      SQLHelper.dbConfig.db.run(
        projects.filter(_.id === projectId).map(_.count_user).result.headOption
      )
    }

  override def toJson(obj: Project): JsValue =
    Json.obj(
      R.id.ID -> obj.id,
      R.id.NAME_PROJECT -> obj.name,
      R.id.MONEY_PROJECT -> obj.money,
      R.id.BUDGET_PROJECT -> obj.budget,
      R.id.TOWN_PROJECT -> obj.town,
      R.id.COUNT_USERS_PROJECT -> obj.countUser,
      R.id.DESCRIPRION_PROJECT -> obj.description,
      R.id.WHO_CREATED_EMAIL_PROJECT -> obj.whoCreatedEmail
    )

  override def fromJson(js: JsValue): Project =
    JsonUtil.parseRequestString(js)(Array(
      Path(R.id.NAME_PROJECT),
      Path(R.id.DESCRIPRION_PROJECT),
      Path(R.id.MONEY_PROJECT),
      Path(R.id.BUDGET_PROJECT),
      Path(R.id.TOWN_PROJECT)
    )) match {
      case Array(_1, _2, _3, _4, _5) => Project(0, _1.as[String],
                                                       _2.as[String],
                                                       _3.as[Int],
                                                       _4.as[Int], 0,
                                                       _5.as[Int], 0, "")
    }
}
