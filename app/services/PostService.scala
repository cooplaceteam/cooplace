package services

import models._
import models.json.{JsonParseObject, JsonUtil, Path}
import play.api.libs.json.{JsValue, Json}
import services.resources.R
import slick.driver.MySQLDriver.api._

object PostService extends JsonParseObject[Post] {
  var posts = TableQuery[PostTable]


  private val postById = for(id <- Parameters[Long]; p <- posts if p.id === id) yield p

  def getPosts() = SQLHelper.dbConfig.db.run(posts.sortBy(_.id).result)


  def add(post: Post) =
    SQLHelper.dbConfig.db.run(
      (posts returning posts.map(_.id)) += post
    )


  def getPostById(postId: Long) = SQLHelper.dbConfig.db.run(postById(postId).result)

  def delete(postId: Long) = {
    SQLHelper.dbConfig.db.run(
      posts.filter((post) => post.id === postId).delete
    )
  }

  override def toJson(obj: Post): JsValue =
    Json.obj(
      R.id.ID -> obj.id,
      R.id.OWNER_ID_POST -> obj.ownerId,
      R.id.PUBLIC_STATUS_POST -> obj.publicStatus,
      R.id.FROM_GROUP_STATUS_POST -> obj.fromGroup,
      R.id.PUBLISH_DATE_POST -> obj.publishDate,
      R.id.MESSAGE_POST -> obj.message
    )
  override def fromJson(js: JsValue): Post =
    JsonUtil.parseRequestString(js)(Array(
      Path(R.id.PUBLIC_STATUS_POST),
      Path(R.id.FROM_GROUP_STATUS_POST),
      Path(R.id.MESSAGE_POST)
    )) match {
      case Array(_1, _2, _3) => Post(0, 0, _1.as[Boolean],
                                              _2.as[Boolean], "",
                                              _3.as[String])
    }
}
