package models

import services.resources.R
import slick.driver.MySQLDriver.api._

// TODO TOWN ^_^
case class
           Project(id: Long,
                   name: String,
                   description: String,
                   money: Int,
                   budget: Int,
                   var countUser: Int,           // will be changed
                   town: Int,
                   var whoCreatedId: Long,       // will be changed
                   var whoCreatedEmail: String)  // will be changed

class ProjectTable(tag: Tag) extends Table[Project](tag, R.tables.PROJECT_TABLE) {
  def id = column[Long](R.id.ID, O.PrimaryKey, O.AutoInc)
  def name = column[String](R.id.NAME_PROJECT)
  def description = column[String](R.id.DESCRIPRION_PROJECT)
  def money = column[Int](R.id.MONEY_PROJECT)
  def budget = column[Int](R.id.BUDGET_PROJECT)
  def count_user = column[Int](R.id.COUNT_USERS_PROJECT)
  def town = column[Int](R.id.TOWN_PROJECT)
  def who_created_id = column[Long](R.id.WHO_CREATED_ID_PROJECT)
  def who_created_email = column[String](R.id.WHO_CREATED_EMAIL_PROJECT)

  def * = (id, name, description, money, budget, count_user, town, who_created_id, who_created_email) <> (Project.tupled, Project.unapply)
}