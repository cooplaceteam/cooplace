package models

import javax.inject.Inject

import play.api.libs.mailer._
import org.apache.commons.mail.EmailAttachment

// TODO send email
class Email @Inject() (mailerClient: MailerClient) {
  def sendEmail(toEmail: String, subject: String, msg: String) = {
    val email = Email(
      "Email",
      "Mister FROM <>",
      Seq(s"Miss TO <$toEmail>"),
      bodyText = Some(subject),
      bodyHtml = Some(msg)
    )
    mailerClient.send(email)
  }
}
