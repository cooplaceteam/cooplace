package models.utils

object Email extends Validator {

  def normalize(email: String) = email
  def validation(email: String) = email.contains("@")

}
