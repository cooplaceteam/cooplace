package models.utils

trait Validator {
  def normalize(validating: String): String
  def validation(validating: String): Boolean
}
