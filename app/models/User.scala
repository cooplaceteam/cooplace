package models


import services.resources.R
import slick.driver.MySQLDriver.api._

case class User(id: Long,
                name: String,
                email: String,
                phone: String,
                password: String)

class UserTable(tag: Tag) extends Table[User](tag, R.tables.USERS_TABLE) {
  def id = column[Long](R.id.ID, O.PrimaryKey, O.AutoInc)
  def name = column[String](R.id.NAME_USER)
  def email = column[String](R.id.EMAIL_USER)
  def phone = column[String](R.id.PHONE_USER)
  def password = column[String](R.id.PASSWORD_USER)

  override def * = (id, name, email, phone, password) <> (User.tupled, User.unapply)

  def name_idx = index("name_idx", name, unique = true) /* TODO */
}

