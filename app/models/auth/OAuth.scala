package models.auth

import models.User

import scala.collection.mutable.HashMap

class OAuth(val client_id: String, val client_secret: String, val lang: String) {

}

object OAuth {
  val nowUsers = new HashMap[String, User]()

  def getByToken(token: String): Option[User] = {
    if(!nowUsers.contains(token)) None
    else
      nowUsers.get(token)
  }
}
