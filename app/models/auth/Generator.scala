package models.auth

import org.apache.commons.codec.digest.DigestUtils

class Generator {
  private def getToken() = {
    val nowTime = System.currentTimeMillis()
    Generator.generateHash(nowTime + Generator.clientSecret)
  }
}

object Generator {
  private val me: Generator = new Generator
  private val clientSecret: String = "meatsecret"

  def getInstance() = me

  def generateHash(text: String) = {
    DigestUtils.md5Hex(text)
  }

}
