package models.json

import scala.concurrent._
import ExecutionContext.Implicits.global
import javax.inject._

import models.json.{JsonUtil, ResponseCode}
import play.api.libs.json.JsValue
import play.api.mvc.{Action, Controller}
import services.UserService
import services.resources.R

import scala.util.Try

object ResponseHelper extends Controller {
  def makeResponseError(implCode: ImplCode) = Ok(JsonUtil.responseString(implCode))
  def makeResponseErrorWithFuture(implCode: ImplCode) = Future {
    Ok(JsonUtil.responseString(implCode))
  }
}
