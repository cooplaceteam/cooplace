package models.json

import models.User
import play.api.libs.json
import play.api.libs.json.{JsLookupResult, JsObject, JsValue, Json}

case class Path(paths: List[String])

object Path {
  def apply(arg: String*): Path  = Path((ImplPath.toList compose ((el: Seq[String]) => for(bit <- el) yield bit))(arg))

  object ImplPath {
    val toList = (el: Seq[String]) => el.toList
  }
}

object JsonUtil {
  def responseString(status: ImplCode, jsonResponse: JsValue = Json.obj()) =
    Json.obj(
      "status" -> status.code,
      "data" -> (if(jsonResponse != Json.obj()) jsonResponse else Json.obj(
        "message" -> status.message
      ))
    );

  // TODO Ask about Map[String, String]
  def toJsonObject(hashMap: Map[String, String]) = Json.toJson(hashMap)

  def parseRequestString(js: JsValue)(need: Array[Path]) = {
    for(path <- need
        if path.paths != null;
        sweet = path.paths) yield {
      sweet.foldLeft(js)((acc, bit) => (acc \ bit).get)
    }
  }

  def parseRequestString(requst: String): (Array[Path]) => Array[JsValue] = parseRequestString(Json.parse(requst))
}
