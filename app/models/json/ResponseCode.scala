package models.json

case class ImplCode(code: Int, message: String)

object ResponseCode extends Enumeration {
  val OK = ImplCode(0, "OK")
  val WRONG_PARAMETERS_IN_JSON = ImplCode(1, "Wrong parameters in json (input)")
  
  val WRONG_LOGIN_OR_PASSWORD = ImplCode(2, "Wrong login or password")
  val WRONG_PERMISSION = ImplCode(3, "Wrong permission (login, please)")

  val WRONG_NO_ACCESS_RIGHTS = ImplCode(4, "Wrong in access (change user)")
}
