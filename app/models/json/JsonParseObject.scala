package models.json

import play.api.libs.json.{JsObject, JsValue}

trait JsonParseObject[T] {
  def toJson(obj: T): JsValue
  def fromJson(js: JsValue): T
}
