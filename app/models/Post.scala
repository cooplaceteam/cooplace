package models

import play.mvc.BodyParser.Json
import services.resources.R
import slick.driver.MySQLDriver.api._

case class Post (id: Long,
                 var ownerId: Long,
                 publicStatus: Boolean,
                 fromGroup: Boolean,
                 var publishDate: String,
                 message: String)

class PostTable(tag: Tag) extends Table[Post](tag, "post") {
  def id = column[Long](R.id.ID, O.PrimaryKey, O.AutoInc)

  def ownerId = column[Long](R.id.OWNER_ID_POST)
  def publicStatus = column[Boolean](R.id.PUBLIC_STATUS_POST)
  def fromGroup = column[Boolean](R.id.FROM_GROUP_STATUS_POST)
  def publishDate = column[String](R.id.PUBLISH_DATE_POST)
  def message = column[String](R.id.MESSAGE_POST)

  override def * = (id, ownerId, publicStatus, fromGroup, publishDate, message) <>(Post.tupled, Post.unapply)
}



