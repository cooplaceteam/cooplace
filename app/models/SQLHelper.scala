package models

import javax.inject.Inject

import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

object SQLHelper {
  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
}
