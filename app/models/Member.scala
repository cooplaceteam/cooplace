package models

import services.{ProjectService, UserService}
import services.resources.R
import slick.driver.MySQLDriver.api._

class MemberTable(tag: Tag) extends Table[(Long, String, Long)](tag, "member") {
  def project_id = column[Long](R.id.PROJECT_ID_MEMBER)
  def description = column[String](R.id.ROLE_MEMBER)
  def user_id = column[Long](R.id.USER_ID_MEMBER)
  def * = (project_id, description, user_id)

  def user = foreignKey("f_user", user_id, UserService.users)(_.id)
  def project = foreignKey("f_project", project_id, ProjectService.projects)(_.id)
}
