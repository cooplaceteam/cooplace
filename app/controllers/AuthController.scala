package controllers

/**
  * TODO ???
  */

import scala.concurrent._
import ExecutionContext.Implicits.global
import javax.inject._

import models.json.{JsonUtil, ResponseCode}
import play.api.libs.json.JsValue
import play.api.mvc.{Action, Controller}
import services.UserService
import services.resources.R

import scala.util.Try

@Singleton
class AuthController@Inject() extends Controller {

  /**
    * {"name": "NAME", "password": "PASSWORD"}
    */
  def login  = Action.async(parse.json) { request =>


    val future = (ImplAuth.getName _ andThen UserService.getByName)(request.body)

    future.map { f =>

      f map { i =>
        if(i.isEmpty)
          Ok(JsonUtil.responseString(ResponseCode.WRONG_LOGIN_OR_PASSWORD))
        else
          ImplAuth.getPassword(request.body) map { pass =>
            if (pass == i.head.password)
              Ok(JsonUtil.responseString(ResponseCode.OK)).withSession(
                R.auth.AUTH -> (i.head.id + "")
              )
            else
              Ok(JsonUtil.responseString(ResponseCode.WRONG_LOGIN_OR_PASSWORD))

          } getOrElse Ok(JsonUtil.responseString(ResponseCode.WRONG_PARAMETERS_IN_JSON))

      }

    }.getOrElse(Future(Ok(JsonUtil.responseString(ResponseCode.WRONG_PARAMETERS_IN_JSON))))

  }

  def logout = Action(parse.anyContent) { request =>
    Ok(request.session.get(R.auth.AUTH).get/* */).withNewSession
  }

  object ImplAuth {

    def getName(js: JsValue) = Try { (js \ R.auth.NAME).get.asOpt[String] } recover {
      case _ => None
    } get

    def getPassword(js: JsValue) = Try { (js \ R.auth.PASSWORD).get.asOpt[String] } recover {
      case _ => None
    } get

  }
}
