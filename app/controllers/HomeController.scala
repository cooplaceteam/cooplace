package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject() extends Controller {

  def index = Action {
    GlobalEnv.a += 1

    Ok("You hi !!! " + GlobalEnv.a)
  }

}

object GlobalEnv {
  var a: Int = 5
}
