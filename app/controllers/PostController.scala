package controllers

import java.text.SimpleDateFormat
import javax.inject.Inject

import com.fasterxml.jackson.databind.deser.std.DateDeserializers.CalendarDeserializer
import play.api.mvc.Controller
import models.Post
import models.json.ResponseHelper


import models.json.{JsonUtil, Path, ResponseCode}
import java.util.Calendar
import play.api._
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsArray, Json}
import services.{PostService, UserService}
import services.resources.R


class PostController @Inject() extends Controller{

  def index = Action.async {
    val future = PostService.getPosts()

    future map { i =>
      Ok(JsonUtil.responseString(ResponseCode.OK, Json.obj(
        "posts" -> i.foldLeft(Json.arr())((acc, el) => acc :+ PostService.toJson(el))))
      )
    }
  }

  def get(id: Long) = Action.async {
    val future = PostService.getPostById(id)

    future map { i =>
      val project = i.head
      Ok(JsonUtil.responseString(ResponseCode.OK, PostService.toJson(project)))
    }
  }
  /**
    * {"publicStatus": true, "fromGroup": false, message = "MESSAGE"}
    */


  def add = Action.async(parse.json) { request =>
    request.session.get(R.auth.AUTH).map { uid =>
      UserService.getById(uid.toLong) flatMap { user =>
        val future = (PostService.fromJson _ andThen ((post: Post) => {
          post.ownerId = user.head.id
          val today = Calendar.getInstance().getTime()
          val timeFormat = new SimpleDateFormat("yyyy-MM-dd") // ("ТHH:mm:ss") for time
          val time = timeFormat.format(today)
          post.publishDate = time
          post
        }) andThen PostService.add)(request.body)
        future map { i =>
          Ok(JsonUtil.responseString(ResponseCode.OK))
        }
      }
    } getOrElse ResponseHelper.makeResponseErrorWithFuture(ResponseCode.WRONG_PERMISSION)
  }

  def delete(id: Long) = Action.async(parse.json) { request =>
    request.session.get(R.auth.AUTH).map { uid =>
      UserService.getById(uid.toLong) flatMap { user =>
        val f = PostService.getPostById(id)
        f flatMap { i =>
          if (i.head.ownerId != user.head.id) {
            ResponseHelper.makeResponseErrorWithFuture(ResponseCode.WRONG_NO_ACCESS_RIGHTS)
          }
          else {
            val future = PostService.delete(id)
            future map { i =>
              Ok(JsonUtil.responseString(ResponseCode.OK))
            }
          }
        }
      }
    } getOrElse ResponseHelper.makeResponseErrorWithFuture(ResponseCode.WRONG_PERMISSION)
  }
/* without WRONG IN ACCESS */
/*  def delete(id: Long) = Action.async(parse.json) { request =>
    request.session.get(R.auth.AUTH).map { uid =>
      UserService.getById(uid.toLong) flatMap { user =>
        val future = PostService.delete(id, user.head)
        future map { i =>
          Ok(JsonUtil.responseString(ResponseCode.OK))
        }
      }
    } getOrElse ResponseHelper.makeResponseErrorWithFuture(ResponseCode.WRONG_PERMISSION)
  }
*/

}



