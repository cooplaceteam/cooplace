package controllers

import models.User
import javax.inject._

import models.json.{JsonUtil, Path, ResponseCode}
import play.api._
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsArray, Json}
import services.UserService
import services.resources.R


@Singleton
class UserController @Inject() extends Controller {

  def index = Action.async(parse.anyContent) { request =>
    val filter = request.getQueryString(R.query.FILTER_QUERY)

    val future = UserService.getAllUsers(filter)
    future map { i =>
      val res = i.foldLeft(Json.arr())((acc, v) => acc :+ UserService.toJson(v))
      Ok(JsonUtil.responseString(ResponseCode.OK, res))
    }
  }

  /**
    * {"name": "NAME", "email": "EMAIL", "phone": "PHONE", "password": "PASSWORD"}
    */
  def add = Action.async(parse.json) { request =>

    val future = UserService.add(UserService.fromJson(request.body))

    future map { i =>
      Ok(JsonUtil.responseString(ResponseCode.OK))
    }
  }
}
