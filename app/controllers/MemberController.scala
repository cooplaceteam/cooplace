package controllers

import javax.inject._

import akka.actor.Status.Success
import models.{Project, User}
import models.json.{JsonUtil, ResponseCode}
import play.api.libs.json.Json
import play.api.mvc._
import services.{ProjectService, UserService}
import services.resources.R

import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.Failure

@Singleton
class MemberController @Inject() extends Controller {

  val functionForCommand = (id: Long, uid: String, func: ((Long, User) => Future[Option[Int]]), inc: () => Int) => {
    UserService.getById(uid.toInt) flatMap { user =>
      func(id, user.head) flatMap { i =>
        ProjectService.setCountUser(id, i.get + inc()) map { result =>
          Ok(JsonUtil.responseString(ResponseCode.OK))
        }
      }
    }
  }

  def add(id: Long) = Action.async(parse.anyContent) { request =>
    request.session.get(R.auth.AUTH).map { uid =>
      functionForCommand(id, uid, ProjectService.inCommand(""), () => + 1) // TODO Why?
    } getOrElse {
      Future {
        Ok(JsonUtil.responseString(ResponseCode.WRONG_PERMISSION))
      }
    }
  }

  def delete(id: Long) = Action.async(parse.json) { request =>
    request.session.get(R.auth.AUTH).map { uid =>
      functionForCommand(id, uid, ProjectService.outCommand, () => - 1)

    } getOrElse {
      Future {
        Ok(JsonUtil.responseString(ResponseCode.WRONG_PERMISSION))
      }
    }
  }
}
