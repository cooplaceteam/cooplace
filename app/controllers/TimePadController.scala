package controllers
import javax.inject.Inject

import scala.concurrent.Future
import play.api.mvc.{Action, _}
import play.api.libs.ws._

import scala.concurrent.ExecutionContext
import scala.util.Try


import play.api.mvc.Controller
import models.json.ResponseHelper
import models.json.{JsonUtil, Path, ResponseCode}

import play.api._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsArray, Json}
import services.{ProjectService, Log}
import services.resources.R


class TimePadController @Inject() (ws: WSClient) extends Controller {
  //https://api.timepad.ru/v1/events.json?limit=20&skip=0&cities=Москва,Санкт-Петербург&fields=location&sort=+starts_at
  val request: WSRequest = ws.url("https://api.timepad.ru/v1/events.json")

  def towns(projectId: Long):Future[String] = {
    var res: String = ""
    val future = ProjectService.getProjectById(projectId)
    future map { p =>
      if(p.nonEmpty) {
        res = Try { (R.towns.json \ (p.head.town + "")).get.as[String]}.map { el => el } getOrElse("")
      }
      res
    }
  }

  def tegs(projectId: Long): Future[String] = {
    //Log.i("Start tegs")
    var res: String = ""
    val future = ProjectService.getProjectById(projectId)

    future map { p =>
      if(p.nonEmpty) {
        res = p.head.description
        //Log.i(p.head.id, p.head.description)
        val arr = res.split(" ").filter(p => p(0) == '#').map(p => p.drop(1))
        res = ""
        for (i <- arr) res += "," + i
        res = res.drop(1)
      }
      res
    }
  }

  def index(id: Long) = Action.async {
    val keywords = tegs(id)
    val cities = towns(id)
    //Log.i(keywords)
    //Log.i(cities)
    keywords.flatMap { key =>
      cities.flatMap { city =>
        Log.i(key, "key")
        Log.i(city, "city")
        val complexRequest: WSRequest =
          request.withQueryString("limit" -> "10", "skip" -> "0",
            "keywords" -> key, "cities" -> city, "sort" -> "+starts_at")
        val futureResponse: Future[WSResponse] = complexRequest.get()
        val future = futureResponse
        future map { response =>
          if(city == "") { NotFound(JsonUtil.responseString(ResponseCode.WRONG_ID)) }
          else { Ok(JsonUtil.responseString(ResponseCode.OK, response.json)) }
        }
      }
    }
  }
}


