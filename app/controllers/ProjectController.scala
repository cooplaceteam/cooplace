package controllers

import javax.inject._

import models.Project
import models.json.{JsonUtil, RequestCode, ResponseCode, ResponseHelper}
import play.api.libs.json.Json
import play.api.mvc._
import services.{ProjectService, UserService}
import services.resources.R

import scala.concurrent._
import ExecutionContext.Implicits.global

@Singleton
class ProjectController @Inject() extends Controller {

  def index = Action.async {
    val future = ProjectService.getProjects()

    future map { i =>
      Ok(JsonUtil.responseString(ResponseCode.OK, Json.obj(
        "projects" -> i.foldLeft(Json.arr())((acc, el) => acc :+ ProjectService.toJson(el))))
      )
    }
  }

  def get(id: Long) = Action.async {
    val future = ProjectService.getProjectById(id)

    future map { i =>
      val project = i.head
      Ok(JsonUtil.responseString(ResponseCode.OK, ProjectService.toJson(project)))
    }
  }

  /**
    * {}
    *
    */
  def add = Action.async(parse.json) { request =>
    request.session.get(R.auth.AUTH).map { uid =>
      UserService.getById(uid.toLong) flatMap { user =>
        val future = (ProjectService.fromJson _ andThen ((p: Project) => {
          p.whoCreatedId = user.head.id
          p.whoCreatedEmail = user.head.email
          p.countUser = 1
          p
        }) andThen ProjectService.addProject)(request.body)

        future flatMap { idProject =>
          ProjectService.inCommand(R.roles.CREATOR)(idProject, user.head) map { j =>
            Ok(JsonUtil.responseString(ResponseCode.OK))
          }
        }
      }
    } getOrElse ResponseHelper.makeResponseErrorWithFuture(ResponseCode.WRONG_PERMISSION)


  }

  def set(id: Long) = Action.async(parse.json) { request =>
    val future = ProjectService.setProjectInfo(id, ProjectService.fromJson(request.body))

    future map { i =>
      Ok(JsonUtil.responseString(ResponseCode.OK))
    }
  }

}
