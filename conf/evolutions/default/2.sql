# projects schema

# --- !Ups

CREATE TABLE `project` (
    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` TEXT NOT NULL,
    `description` TEXT NOT NULL,
    `money` INT NOT NULL,
    `budget` INT NOT NULL,
    `count_users` INT NOT NULL,
    `town` INT NOT NULL,
    `who_created_id` BIGINT NOT NULL,
    `who_created_email` TEXT NOT NULL
);

# --- !Downs

DROP TABLE `project`;