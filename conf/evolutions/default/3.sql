# members schema

# --- !Ups

CREATE TABLE `member` (
    `project_id` BIGINT NOT NULL,
    `role` TEXT NOT NULL,
    `user_id` BIGINT NOT NULL
);

# --- !Downs

DROP TABLE `member`;