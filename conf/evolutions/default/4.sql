# posts schema

# --- !Ups
CREATE TABLE `post` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `ownerId` BIGINT NOT NULL,
  `publicStatus`  INT NOT NULL,
  `fromGroup`  INT NOT NULL,
  `publishDate` DATE NOT NULL,
  `message` TEXT NOT NULL
)

# --- !Downs
DROP TABLE `post`;